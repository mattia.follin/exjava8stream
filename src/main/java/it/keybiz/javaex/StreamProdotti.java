package it.keybiz.javaex;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static it.keybiz.javaex.Prodotti.Categoria.ALIMENTARI;

public class StreamProdotti{

    // mi appoggio ad un factory method che genera a rotazione
    // la stessa lista di libri
    public List<Prodotti> generaListaProdotti(int n) {
        return Stream.generate(Magazziniere::gen)
            .limit(n)
            .collect(Collectors.toList());
    }

    public long contaProdottiDiBellezza(List<Prodotti> list) {
        ArrayList<Prodotti> collect = list
                .stream()
                .filter(prodotto -> prodotto.getCategoria()== Prodotti.Categoria.BELLEZZA)
                .collect(Collectors.toCollection(ArrayList::new));
                return collect.size();
    }

    public List<Prodotti> prezzoCompresoTra12e15(List<Prodotti> list) {
        ArrayList<Prodotti> nome = list
                .stream()
                .filter(Prodotto -> Prodotto.getPrezzo()>= 12 && Prodotto.getPrezzo()<15 )
                .collect(Collectors.toCollection(ArrayList::new));
                return nome;
    }

    public List<String> filtraListaProdottiAlimentariEBellezza(List<Prodotti> list) {
        ArrayList<Prodotti> wow = (ArrayList<Prodotti>) list
                .stream()
                .filter(prodotto -> prodotto.getCategoria().equals(Prodotti.Categoria.BELLEZZA) || prodotto.getCategoria().equals(ALIMENTARI))
                .collect(Collectors.toCollection(ArrayList::new));
                List<String> collect = wow.stream().map(Prodotti::getTitolo).collect(Collectors.toList());
                return collect;
    }

    public List<Prodotti> generaListaProdottiAlimentari(int n) {
        return Stream.generate(() -> new Prodotti("Prodotto alimentare", ALIMENTARI, 10))
                .limit(n)
                .collect(Collectors.toList());
    }

    public boolean checkSePresenteBurningChrome(List<Prodotti> list) {
        boolean presente = list
                .stream()
                .noneMatch(prodotto -> prodotto.getTitolo().equalsIgnoreCase("Burning Chrome"));
                return false;
    }

    public int sommaCosti_reduce(List<Prodotti> list) {
        ArrayList<Prodotti> reduce = (ArrayList<Prodotti>) list;
                List<Integer> collect = reduce.stream().map(Prodotti::getPrezzo).collect(Collectors.toList());
        Integer myResult = collect.stream().reduce(5, Integer::sum);
        return myResult - 5;
    }

    public int sommaCosti_sum(List<Prodotti> list) {
        ArrayList<Prodotti> reduce = (ArrayList<Prodotti>) list;
        List<Integer> collect = reduce.stream().map(Prodotti::getPrezzo).collect(Collectors.toList());
        Integer myResult = collect.stream().reduce(5, Integer::sum);
        return myResult - 5;
    }

    public double sommaCostiInDollari(double EUR_USD, List<Prodotti> list) {
        ArrayList<Prodotti> reduce = (ArrayList<Prodotti>) list;
        List<Integer> collect = reduce.stream().map(Prodotti::getPrezzo).collect(Collectors.toList());
        Integer myResult = collect.stream().reduce(5, Integer::sum);
        return (myResult - 5) * 1.12;
    }

    public Optional<Prodotti> prodottoMenoCaroDa12InSu(List<Prodotti> list) {
        int minPrice = 12;
        ArrayList<Prodotti> collect = list
                .stream()
                .filter(p -> p.getPrezzo()==12)
                .collect(Collectors.toCollection(ArrayList::new));
                return collect.stream().findFirst();
    }

    public List<Prodotti> prodottoOrdinatiPerPrezzo(List<Prodotti> list) {
        List<Prodotti> sorted = (ArrayList<Prodotti>) list
                .stream()
                .sorted(Comparator.comparing(Prodotti::getPrezzo))
                .collect(Collectors.toList());
                return sorted;
    }

    // Titolo: "Pozione Harry Potter 1" "Pozione Harry Potter 2"... "Pozione Harry Potter n"
    // categoria: ALIMENTARI, prezzo: 15 euro
    public List<Prodotti> generaPozioneHarryPotterDa15Euro(int n) {
        List<Prodotti> list = new ArrayList<>();
        for (int i=1; i<=n; i++){
            String name = "Harry Potter ";
            name=name + i;
            list.add(new Prodotti(name,ALIMENTARI,15));
        }
        return list;
    }

    public List<Prodotti> mescolaLista(List<Prodotti> list) {
        List<Prodotti> mescolaLista = new ArrayList<>();
            Collections.shuffle(list);
            return list;
        }

    public Optional<Prodotti> primoPiuCaroDelPrecedente(List<Prodotti> list) {
        List <Prodotti> primoPiuCaroDelPrecedente = new ArrayList<>();
        return list
                .stream()
                .sorted(Comparator.comparingInt(Prodotti::getPrezzo).reversed())
                .collect(Collectors.toCollection(ArrayList::new))
                .stream()
                .findFirst();
    }

}
